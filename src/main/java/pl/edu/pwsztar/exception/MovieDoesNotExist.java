package pl.edu.pwsztar.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "movie does not exist")
public class MovieDoesNotExist extends RuntimeException{
}
